import { bot } from "./services/index.js";
import { getAllMessages } from './dbConnections/index.js';

const reply = async () => {
    const results = await getAllMessages();
    console.log(results);
    bot.hears((text, ctx) => {
        let answers = [];
        results.forEach(data => {
            var leftReg = `.*${data.text.toLowerCase()}.*`;
            var rightReg = `.*${text.toLowerCase()}.*`;
            var leftText;
            var rightText;
            let isRegexed = false;
            try {
                leftText = new RegExp(leftReg);
                rightText = new RegExp(rightReg);
                isRegexed = true
            } catch(ex) {
                console.log("Catched invalid charactors!")
            }
            if(isRegexed && (leftText.test(text.toLowerCase()) || rightText.test(data.text.toLowerCase()))) {
                answers.push(data.rep.toLowerCase());
            }
        });
        if(answers.length) {
            const randomRepIndex = Math.floor(Math.random() * answers.length);
            ctx.reply(answers[randomRepIndex], {
                reply_to_message_id: ctx.message.message_id
            });
        }
    })
}

reply();

// connection.connect(function(err) {
//     if (err) {
//           return console.error('error: ' + err);
//     }

//     console.log('Connected to the bot server!');

//     bot.start((ctx) => ctx.reply("Hello there! Welcome to Hieu Duong's bot!"))

    

//     bot.action('teach', ctx => {
//         bot.telegram.sendMessage(ctx.chat.id, "Để dạy thêm từ vựng, hoặc dạy em chửi nhau, sếp hãy gõ:\n /teach <từ_cần_dạy> <câu_trả_lời> \n Ví dụ: /teach <chào bạn> <chào bạn, bạn khỏe không?>")
//     })

//     bot.action('howToUse', ctx => {
//         bot.telegram.sendMessage(ctx.chat.id, "Hiện tại, bot sẽ tự động trả lời theo như những gì người dùng dạy nó, để dạy bot cách trả lời, bạn hãy chọn dạy thêm cho bot, rồi sau đó khi bạn chat gì, bot sẽ tự động trả lời!")
//     })
    
//     bot.command('bot', ctx => {
//         let animalMessage = `Sếp gọi em phải không, sếp muốn làm gì nhỉ?`;
//         bot.telegram.sendMessage(ctx.chat.id, animalMessage, {
//             reply_markup: {
//                 inline_keyboard: [
//                     [{
//                             text: "Dạy thêm cho bot",
//                             callback_data: "teach"
//                         },
//                         {
//                             text: "Cách sử dụng bot",
//                             callback_data: 'howToUse'
//                         }
//                     ],
//                 ]
//             }
//         })
//     })

//     const uuidv4 = () => {
//         return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
//           var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
//           return v.toString(16);
//         });
//     }

//     const addToBotReplies = (text, user_id) => {
//         const replie = takeMessage(text)
//         if(!replie.text||!replie.rep) {
//             return "Không được để trống phần câu hỏi hoặc câu trả lời!"
//         }
//         var sqlInsert = `INSERT INTO bot_reply (id, text, rep, user_id) VALUES ('${uuidv4()}', '${replie.text}', '${replie.rep}', '${user_id}')`;
//         connection.query(sqlInsert, (error, result) => {
//             if(err) {console.log(error)} else {
//                 console.log(result)
//             }
//         })
//         return ""
//     }

//     const takeMessage = (text) => {
//         const t = /\/teach <(.*)> <(.*)>/g.exec(text) ? /\/teach <(.*)> <(.*)>/g.exec(text)[1] : "";
//         const r = /\/teach <(.*)> <(.*)>/g.exec(text) ? /\/teach <(.*)> <(.*)>/g.exec(text)[2] : "";
//         let replie = {
//             text: t,
//             rep: r
//         }
//         return replie
//     }

//     bot.command('teach', ctx => {
//         const isSuccess = addToBotReplies(ctx.update.message.text, ctx.update.message.from.id)
//         if(!isSuccess) {
//             callBotReplies()
//             let animalMessage = `Done sếp ơi, đã lĩnh ngộ tinh hoa của sếp, hehe`;
//             bot.telegram.sendMessage(ctx.chat.id, animalMessage)
//         } else {
//             bot.telegram.sendMessage(ctx.chat.id, isSuccess)
//         }
//     })

//     bot.launch();

// });