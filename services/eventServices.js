bot.command("event-new", ctx => {
    const numberFormat = /\/event200 (?<number>\d{2})/g.exec(ctx.update.message.text) ? /\/event-new (?<number>\d{2})/g.exec(ctx.update.message.text).groups.number : "";
    if(!numberFormat) {
        ctx.reply("Sai cú pháp hoặc dữ liệu bình chọn không hợp lệ.\nLưu ý: cú pháp bình chọn là \"/event200 <số dự đoán>\"\nsố dự đoán phải là số có 2 chữ số và nằm trong khoảng từ 00 đến 99\nví dụ, để bình chọn số 44 nhắn lên group với cú pháp \"/event200 44\"", {
            reply_to_message_id: ctx.message.message_id
        });
        return
    }
    const curentDate = new Date(1000*ctx.update.message.date);
    const convertedDate = `${curentDate.getFullYear()}.${curentDate.getMonth()}.${curentDate.getDay()} ${curentDate.getHours()}:${curentDate.getMinutes()}:${curentDate.getSeconds()}`
    var sqlQuery = "SELECT * FROM event";
    connection.query(sqlQuery, function (err, results) {
        if(err) {console.log(err)} else {
            let voted = false;
            results.forEach(re => {
                if(ctx.update.message.from.id == re.user_id) {
                    ctx.reply("Dân chơi đã bình chọn rồi, tính chơi ăn gian hay gì, bế lên phường giờ -_-", {
                        reply_to_message_id: ctx.message.message_id
                    });
                    voted = true
                }
            })

            if(voted) {
                return;
            } else {
                var sqlInsert = `INSERT INTO event (user_id, time, number, user_name) VALUES ('${ctx.update.message.from.id}', '${convertedDate}', '${numberFormat}', '${ctx.update.message.from.username}')`;
                    connection.query(sqlInsert, (error, result) => {
                    if(error) {
                        ctx.reply("Có lỗi khi bình chọn! vui lòng bình chọn lại!" + error + ", " + result, {
                            reply_to_message_id: ctx.message.message_id
                        });
                    } else {
                        ctx.reply("Ok, dân chơi đã bình chọn số " + numberFormat + ", vui lòng chờ đến 6h30 tối để xem ông nào được bế lên phường :D", {
                            reply_to_message_id: ctx.message.message_id
                        });
                    }
                })
            }
        }
    })
})

const dateTimeSort = (array) => {
    for(let i = 0; i < array.length -1 ; i++) {
        for(let j = 0; j < array.length - i - 1; j++) {
            if (Date.parse(array[j].time) > Date.parse(array[j + 1].time)) {
                let temp = array[j];
                array[j] = array[j + 1];
                array[j + 1] = temp;
            }
        }
    }
    return array;
}

bot.command("ketquaevent", ctx => {
    ctx.reply("Đã nhận lệnh, đang tham chiếu kết quả cuối cùng và kết quả các dân chơi đã bình chọn...");
    const number = /\/ketquaevent (?<number>\d{2})/g.exec(ctx.update.message.text) ? /\/ketquaevent (?<number>\d{2})/g.exec(ctx.update.message.text).groups.number : "";
    var sqlQuery = "SELECT * FROM event";
    let trunggiai;
    let gannhat;
    return connection.query(sqlQuery, function (err, results) {
        if(err) {console.log(err)} else {
            trunggiai = dateTimeSort(results.filter( t => t.number == number));
            const indexArr = results.map((num) => {
                return Math.abs(num.number - number)
            })
            const min = Math.min(...indexArr)
            gannhat = results[indexArr.indexOf(min)]
            if(trunggiai.length) {
                ctx.reply(`Chúc mừng dân chơi [${trunggiai[0].user_name}](tg://user?id=${trunggiai[0].user_id}) đã trúng giải của sự kiện lần này!!\nVui lòng liên hệ cán bộ [Hiếu Dương](tg://user?id=956841299) để nhận giải`, {
                    "parse_mode": "Markdown"
                })
            } else if(gannhat) {
                ctx.reply(`Chúc mừng dân chơi [${gannhat.user_name}](tg://user?id=${gannhat.user_id}) đã trúng giải của sự kiện lần này!!\n Vui lòng liên hệ cán bộ [Hiếu Dương](tg://user?id=956841299) để nhận giải`, {
                    "parse_mode": "Markdown"
                })
            } else {
                ctx.reply("Tiếc quá, không có dân chơi nào ăn giải lần này, tiền lại vè túi cán bộ rồi :D");
            }
        }
    })
})