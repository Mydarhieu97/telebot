import {bot} from './botCreating.js';

const start = (msg) => bot.start((ctx) => ctx.reply(msg));

const launch = bot.launch();

const sendMessage = (msg) => bot.telegram.sendMessage(ctx.chat.id, msg);

const commands = (command, msg, markup = undefined) => bot.command(command, ctx => {
    bot.telegram.sendMessage(ctx.chat.id, msg, markup ? {
        reply_markup: markup.reply_markup
    } : {})
});

const action = (action, msg) => bot.action(action, ctx => {
    bot.telegram.sendMessage(ctx.chat.id, msg);
})